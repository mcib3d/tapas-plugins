package mcib3d.tapas.IJ.plugins.misc;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.gui.Roi;
import ij.plugin.Converter;
import ij.plugin.LutLoader;
import ij.plugin.filter.ThresholdToSelection;
import ij.process.ByteProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import ij.process.LUT;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DIntLabelImage;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.awt.*;
import java.awt.image.IndexColorModel;
import java.util.HashMap;

public class DrawRoiProcess implements TapasProcessingIJ {
    private static final String DIR_RAW = "dirRaw";
    private static final String FILE_RAW = "fileRaw";

    HashMap<String, String> parameters;
    ImageInfo info;

    public DrawRoiProcess() {
        parameters = new HashMap<>();
        setParameter(DIR_RAW, "?ij?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR_RAW:
                parameters.put(id, value);
                return true;
            case FILE_RAW:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        // open raw image
        String nameF = TapasBatchUtils.analyseFileName(parameters.get(FILE_RAW), info);
        String dirF = TapasBatchUtils.analyseDirName(parameters.get(DIR_RAW));
        // check link
        ImageInfo link = TapasBatchProcess.getLink(nameF);
        System.out.println("LINK " + link);
        // open image raw
        ImageHandler raw;
        if (link != null) {
            IJ.log("Opening link " + link);
            raw = ImageHandler.wrap(TapasBatchProcess.inputImage(link));
        } else {
            raw = ImageHandler.wrap(IJ.openImage(dirF + nameF));
            IJ.log("Opening local file " + dirF + nameF);
        }
        ImagePlus drawPlus = raw.getImagePlus();
        ImageStack drawStack = drawPlus.getStack();

        Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(imagePlus));
        System.out.println("Found " + population.getNbObjects() + " objects");
        for (Object3DInt object3DInt : population.getObjects3DInt()) {
            ImageHandler labelImageCrop = new Object3DIntLabelImage(object3DInt).getCroppedLabelImage(255);
            int offX = labelImageCrop.offsetX;
            int offY = labelImageCrop.offsetY;
            int offZ = labelImageCrop.offsetZ;
            // extract selections crop
            ImageByte binaryCrop = labelImageCrop.thresholdAboveExclusive(0);
            for (int zz = 0; zz < labelImageCrop.sizeZ; zz++) {
                ByteProcessor mask = new ByteProcessor(binaryCrop.sizeX, binaryCrop.sizeY, (byte[]) binaryCrop.getArray1D(zz));
                mask.setThreshold(1, 255, ImageProcessor.NO_LUT_UPDATE);
                ImagePlus maskPlus = new ImagePlus("mask " + zz, mask);
                ThresholdToSelection tts = new ThresholdToSelection();
                tts.showStatus(false);
                tts.setup("", maskPlus);
                tts.run(mask);
                Roi roiTmp = maskPlus.getRoi();
                if (roiTmp != null) {
                    roiTmp.setLocation(roiTmp.getXBase() + offX, roiTmp.getYBase() + offY);
                    // test draw
                    ImageProcessor processor;
                    if (labelImageCrop.sizeZ > 1) processor = drawStack.getProcessor(zz + offZ + 1);
                    else processor = drawPlus.getProcessor();
                    processor.setColor(Color.WHITE);
                    roiTmp.drawPixels(processor);
                }
            }
        }

        return drawPlus;
    }

    @Override
    public String getName() {
        return "Draw Rois on raw image";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR_RAW, FILE_RAW};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }

    public static void main(String[] args) {
        ImagePlus plus = IJ.openImage("/home/thomas/filtered-2.tif");
        ImageHandler handler = ImageHandler.wrap(plus);
        ImageHandler draw = handler.createSameDimensions();
        ImagePlus drawPlus = draw.getImagePlus();
        ImageStack stackPlus = drawPlus.getStack();

        ImageConverter converter = new ImageConverter(drawPlus);
        converter.convertToRGB();

        drawPlus.show();
    }
}
