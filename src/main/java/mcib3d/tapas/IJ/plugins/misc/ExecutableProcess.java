package mcib3d.tapas.IJ.plugins.misc;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.tapas.IJ.TapasProcessingIJ;

import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.*;
import java.util.HashMap;

public class ExecutableProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";
    private static final String ARG = "arg";

    HashMap<String, String> parameters;
    ImageInfo info;

    public ExecutableProcess() {
        parameters = new HashMap<>();
        parameters.put(DIR, "");
        parameters.put(ARG, "");
        info = new ImageInfo();
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
            case ARG:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // analyse DIR and ARG
        String dir = parameters.get(DIR);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        String exe = parameters.get(FILE);
        String arg = parameters.get(ARG);
        String arg2 = TapasBatchUtils.analyseStringKeywords(arg, info);
        try {
            // create script file
            String tmpExe = "";
            if (IJ.isWindows()) tmpExe = System.getProperty("user.home") + File.separator + "tmpExeTapas.bat";
            if (IJ.isLinux()) tmpExe = System.getProperty("user.home") + File.separator + "tmpExeTapas.sh";
            if (IJ.isMacOSX()) tmpExe = System.getProperty("user.home") + File.separator + "tmpExeTapas.command";
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(tmpExe));
            bufferedWriter.write("\""+dir2 + exe + "\" " + arg2);
            bufferedWriter.close();

            // Process
            ProcessBuilder pb = null;
            if (IJ.isLinux()) {
                pb = new ProcessBuilder("bash", tmpExe);
            }
            if (IJ.isWindows()) {
                pb = new ProcessBuilder(tmpExe);
            }
            if (IJ.isMacOSX()) {
                pb = new ProcessBuilder(tmpExe);
            }

            final Process pTest = pb.start();
            if (pTest.waitFor() == 0)
                IJ.log("Process "+exe+" terminated correctly");
            else
                IJ.log("Process "+exe+" terminated in error");
        } catch (IOException e) {
            IJ.log("Pb with exe (1) " + parameters.get(FILE) + " " + e.getMessage());
        } catch (InterruptedException e) {
            IJ.log("Pb with exe (2) " + parameters.get(FILE));
        }

        return TapasBatchProcess.duplicatePlus(input);
    }

    @Override
    public String getName() {
        return "Executable file";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE, ARG};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
