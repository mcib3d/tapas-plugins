package mcib3d.tapas.IJ.plugins.analysis;

import ij.ImagePlus;
import ij.gui.NewImage;
import ij.measure.ResultsTable;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class BinaryClassificationProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";
    private static final String DESC = "descriptor";
    private static final String THRESHOLD = "threshold";

    Map<String, String> parameters;
    ImageInfo info;

    public BinaryClassificationProcess() {
        parameters = new HashMap<>();
        setParameter(DIR,"?ij?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
            case DESC:
                parameters.put(id, value);
                return true;
            case THRESHOLD:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        ResultsTable resultsTable = ResultsTable.getResultsTable();
        if (resultsTable == null) resultsTable = new ResultsTable();
        else resultsTable.reset();
        // reading results
        String name = parameters.get(FILE);
        String dir = parameters.get(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        resultsTable = ResultsTable.open2(dir2 + name2);
        // population
        ImageInt img = ImageInt.wrap(input);
        Objects3DIntPopulation population = new Objects3DIntPopulation(img);
        // results in new Image
        ImagePlus classify = NewImage.createByteImage("classify", input.getWidth(), input.getHeight(), input.getNSlices(), NewImage.FILL_BLACK);
        ImageHandler handler = ImageHandler.wrap(classify);
        // analyse results
        String desc = parameters.get(DESC);
        double threshold = Double.parseDouble(parameters.get(THRESHOLD));
        int c1 = 0;
        int c2 = 0;
        for (int res = 0; res < resultsTable.size(); res++) {
            int value = (int) resultsTable.getValue("Value", res);
            Object3DInt object3D = population.getObjectByLabel(value);
            double descValue = resultsTable.getValue(desc, res);
            if (descValue > threshold) {
                object3D.drawObject(handler, 255);
                c2++;
            } else {
                object3D.drawObject(handler, 128);
                c1++;
            }
        }
        // results table
        resultsTable.reset();
        resultsTable.incrementCounter();
        resultsTable.setValue("Positive", 0, c2);
        resultsTable.setValue("Negative", 0, c1);
        resultsTable.setValue("Total", 0, c1 + c2);
        try {
            resultsTable.saveAs(dir2 + name2 + "-classification.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classify;
    }

    @Override
    public String getName() {
        return "Binary Classification based on results table";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE, DESC, THRESHOLD};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
