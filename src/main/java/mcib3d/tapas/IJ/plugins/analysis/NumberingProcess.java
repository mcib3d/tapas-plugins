package mcib3d.tapas.IJ.plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureNumbering;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NumberingProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";
    private static final String DIR_LABEL = "dirLabel";
    private static final String FILE_LABEL = "fileLabel";
    private boolean debug = true;

    HashMap<String, String> parameters;
    ImageInfo info;

    public NumberingProcess() {
        parameters = new HashMap<>();
        setParameter(DIR_LABEL, "?ij?");
        setParameter(DIR, "?ij?");
    }

    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
            case DIR_LABEL:
                parameters.put(id, value);
                return true;
            case FILE_LABEL:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    public ImagePlus execute(ImagePlus input) {
        if (debug) IJ.log("Opening labelled image");
        String name = parameters.get(FILE);
        String dir = parameters.get(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        // open other label image to perform numbering
        String nameF = TapasBatchUtils.analyseFileName(parameters.get(FILE_LABEL), info);
        String dirF = TapasBatchUtils.analyseDirName(parameters.get(DIR_LABEL));
        // check link
        ImageInfo link = TapasBatchProcess.getLink(nameF);
        System.out.println("LINK " + link);
        // open image other label
        ImageHandler label;
        if (link != null) {
            IJ.log("Opening link " + link);
            label = ImageHandler.wrap(TapasBatchProcess.inputImage(link));
        } else {
            label = ImageHandler.wrap(IJ.openImage(dirF + nameF));
            IJ.log("Opening local file " + dirF + nameF);
        }
        // measurements
        if (debug) IJ.log("Building population");
        Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(input));
        List<String> list = new ArrayList<>();
        list.add("Label");
        list.add("NbObjects");
        list.add("VolObjects");
        list.add("PercObjects");
        saveResults(population, label, input.getTitle(), list, dir2 + name2);

        return TapasBatchProcess.duplicatePlus(input);
    }


    public String getName() {
        return "Numbering inside objects";
    }

    public String[] getParameters() {
        return new String[]{DIR_LABEL, FILE_LABEL, DIR, FILE};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }

    private boolean saveResults(Objects3DIntPopulation population, ImageHandler signal, String title, List<String> list, String path) {
        // buffered writer
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(path));
            Map<String, Double> m = doMeasurement(population.getFirstObject(), signal, list);
            bf.write("imageLabel,imageSignal");
            for (String mes : m.keySet()) {
                bf.write("," + mes);
            }
            for (Object3DInt O : population.getObjects3DInt()) {
                m = doMeasurement(O, signal, list);
                bf.newLine();
                bf.write(title + "," + signal.getImagePlus().getTitle());
                for (String mes : m.keySet()) {
                    bf.write("," + m.get(mes));
                }
                bf.flush();
            }
            bf.close();
        } catch (IOException e) {
            return false;
        }
        // buffered writer

        return true;
    }

    private Map<String, Double> doMeasurement(Object3DInt object3D, ImageHandler signal, List<String> list) {
        Map<String, Double> map = new HashMap<>();
        MeasureNumbering numbering = new MeasureNumbering(object3D, signal);
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i).toLowerCase().trim()) {
                case "label":
                    map.put("Label", (double) object3D.getLabel());
                    break;
                case "nbobjects":
                    map.put("NbObjects", numbering.getValueMeasurement(MeasureNumbering.OBJ_NUMBER));
                    break;
                case "volobjects":
                    map.put("VolObjects", numbering.getValueMeasurement(MeasureNumbering.OBJ_VOLUME));
                    break;
                case "percobjects":
                    map.put("PercObjects", numbering.getValueMeasurement("NumberObjPerc."));// FIXME next version change to static field
                    break;
            }
        }

        return map;
    }
}
