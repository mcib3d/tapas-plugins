package mcib3d.tapas.IJ.plugins.analysis;

import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.concurrent.atomic.AtomicReference;

public class KeepBiggestProcess implements TapasProcessingIJ {

    @Override
    public boolean setParameter(String id, String value) {
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        ImageHandler handler = ImageHandler.wrap(input);
        // if no objects return
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        if (population.getNbObjects() == 0) return input.duplicate();
        /* get biggest object */
        final AtomicReference<Object3DInt> object3DMax = new AtomicReference<>();
        object3DMax.set(population.getFirstObject());
        final AtomicReference<Double> maxVol = new AtomicReference<>();
        maxVol.set(object3DMax.get().size());
        population.getObjects3DInt().forEach(O -> {
            if (O.size() > maxVol.get()) {
                maxVol.set(O.size());
                object3DMax.set(O);
            }
        });

        // draw biggest object in result
        ImageHandler draw = handler.createSameDimensions();
        draw.setScale(handler);
        object3DMax.get().drawObject (draw, 255);

        return draw.getImagePlus();
    }

    @Override
    public String getName() {
        return "Keep biggest object";
    }

    @Override
    public String[] getParameters() {
        return new String[0];
    }

    public String getParameter(String id) {
        return null;
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
    }
}
