package mcib3d.tapas.IJ.plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureIntensity;
import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuantificationProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";
    private static final String LIST_MEASUREMENTS = "list";
    private static final String DIR_RAW = "dirRaw";
    private static final String FILE_RAW = "fileRaw";

    HashMap<String, String> parameters;
    ImageInfo info;

    public QuantificationProcess() {
        parameters = new HashMap<>();
        setParameter(DIR, "?ij?");
        setParameter(DIR_RAW, "?ij?");
        parameters.put(LIST_MEASUREMENTS, "mean");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
            case DIR_RAW:
                parameters.put(id, value);
                return true;
            case FILE_RAW:
                parameters.put(id, value);
                return true;
            case LIST_MEASUREMENTS:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // open raw image to perform quantification
        String nameF = TapasBatchUtils.analyseFileName(parameters.get(FILE_RAW), info);
        String dirF = TapasBatchUtils.analyseDirName(parameters.get(DIR_RAW));
        // check link
        ImageInfo link = TapasBatchProcess.getLink(nameF);
        System.out.println("LINK " + link);
        // open image raw
        ImageHandler raw;
        if (link != null) {
            IJ.log("Opening link " + link);
            raw = ImageHandler.wrap(TapasBatchProcess.inputImage(link));
        } else {
            raw = ImageHandler.wrap(IJ.openImage(dirF + nameF));
            IJ.log("Opening local file " + dirF + nameF);
        }
        // save results
        String name = parameters.get(FILE);
        String dir = parameters.get(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        // measurements
        Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(input));
        if(population.getNbObjects() ==0) {
            IJ.log("No objects to measure");
            return TapasBatchProcess.duplicatePlus(input);
        }
        List<String> list = new ArrayList<>();
        list.add("label");
        String measList = parameters.get(LIST_MEASUREMENTS);
        if (measList.equalsIgnoreCase("all")) {
            measList = "mean,min,max,sd,sum,centre";
        }
        String[] meas = measList.split(",");
        for (int i = 0; i < meas.length; i++) list.add(meas[i].trim().toLowerCase());

        saveResults(population, raw, input.getTitle(), list, dir2 + name2);
        // buffered reader
        /*Results Table
        ResultsTable resultsTable = ResultsTable.getResultsTable();
        if (resultsTable == null) resultsTable = new ResultsTable();
        else resultsTable.reset();
        final ResultsTable rt2 = resultsTable;
        // do measurements
        AtomicInteger ai = new AtomicInteger(0);
        population.getObjects3DInt().forEach(O -> {
            rt2.incrementCounter();
            Map<String, Double> m = doMeasurement(O, raw, list);
            for (String val : m.keySet()) rt2.setValue(val, ai.get(), m.get(val));
            ai.getAndIncrement();
        });

        try {
            resultsTable.saveAs(dir2 + name2);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        return TapasBatchProcess.duplicatePlus(input);
    }

    private boolean saveResults(Objects3DIntPopulation population, ImageHandler signal, String title, List<String> list, String path) {
        // buffered reader
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(path));
            Map<String, Double> m = doMeasurement(population.getFirstObject(), signal, list);
            bf.write("imageLabel,imageSignal");
            for (String mes : m.keySet()) {
                bf.write("," + mes);
            }
            for (Object3DInt O : population.getObjects3DInt()) {
                m = doMeasurement(O, signal, list);
                bf.newLine();
                bf.write(title + "," + signal.getImagePlus().getTitle());
                for (String mes : m.keySet()) {
                    bf.write("," + m.get(mes));
                }
                bf.flush();
            }
            bf.close();
        } catch (IOException e) {
            return false;
        }
        // buffered reader

        return true;
    }

    private Map<String, Double> doMeasurement(Object3DInt object3D, ImageHandler raw, List<String> list) {
        Map<String, Double> map = new HashMap<>();
        MeasureObject measureObject = new MeasureObject(object3D);
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i).toLowerCase().trim()) {
                case "label":
                    map.put("Label", (double) object3D.getLabel());
                    break;
                case "mean":
                    map.put("Mean", measureObject.measureIntensity(MeasureIntensity.INTENSITY_AVG, raw));
                    break;
                case "min":
                    map.put("Min", measureObject.measureIntensity(MeasureIntensity.INTENSITY_MIN, raw));
                    break;
                case "max":
                    map.put("Max", measureObject.measureIntensity(MeasureIntensity.INTENSITY_MAX, raw));
                    break;
                case "sd":
                    map.put("StdDev", measureObject.measureIntensity(MeasureIntensity.INTENSITY_SD, raw));
                    break;
                case "sum":
                    map.put("Sum", measureObject.measureIntensity(MeasureIntensity.INTENSITY_SUM, raw));
                    break;
                case "centre":
                case "center":
                    // map.put("Centre", measureObject.measureIntensity(MeasureIntensity.INTENSITY_CENTROID, raw));
                    break;
            }
        }

        return map;
    }


    @Override
    public String getName() {
        return "Signal quantification";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR_RAW, FILE_RAW, LIST_MEASUREMENTS, DIR, FILE};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
