package mcib3d.tapas.IJ.plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import ij.measure.ResultsTable;
import mcib3d.geom.interactions.InteractionsComputeContours;
import mcib3d.geom.interactions.InteractionsComputeDamLines;
import mcib3d.geom.interactions.InteractionsList;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class InteractionsProcess implements TapasProcessingIJ {
    final private String METHOD = "method";
    private static final String DIR = "dir";
    private static final String FILE = "file";

    ImageInfo info;
    private final Map<String, String> parameters;

    public InteractionsProcess() {
        parameters = new HashMap<>();
        parameters.put(METHOD, "lines");
        parameters.put(DIR,"?ij?");
        parameters.put(FILE, "?image?-interactions.csv");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case METHOD:
                if(value.equalsIgnoreCase("lines")||(value.equalsIgnoreCase("touching")))
                    parameters.put(id,value);
                else
                    IJ.log("Method for separation should be \"lines\" or \"touching\"");
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
            case DIR:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        ImageHandler image = ImageHandler.wrap(input);
        // compute interactions
        InteractionsList list;
        if(getParameter(METHOD).equalsIgnoreCase("touching"))
            list = new InteractionsComputeContours().compute(image);
        else
            list = new InteractionsComputeDamLines().compute(image);
        // results table
        String name = TapasBatchUtils.analyseFileName(parameters.get(FILE), info);
        String dir = TapasBatchUtils.analyseDirName(parameters.get(DIR));
        try {
            ResultsTable resultsTable = list.getResultsTableOnlyColoc();
            resultsTable.saveAs(dir+ File.separator+name);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return TapasBatchProcess.duplicatePlus(input);
    }

    @Override
    public String getName() {
        return "Compute interactions between objects (touching or separated by lines)";
    }

    @Override
    public String[] getParameters() {
        return new String[]{METHOD, DIR, FILE};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
