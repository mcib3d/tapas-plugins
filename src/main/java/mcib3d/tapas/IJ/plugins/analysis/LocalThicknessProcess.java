package mcib3d.tapas.IJ.plugins.analysis;

import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.measure.Calibration;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageByte;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.IJ.plugins.analysis.LT.Clean_Up_Local_Thickness;
import mcib3d.tapas.IJ.plugins.analysis.LT.Distance_Ridge;
import mcib3d.tapas.IJ.plugins.analysis.LT.EDT_S1D;
import mcib3d.tapas.IJ.plugins.analysis.LT.Local_Thickness_Parallel;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;

import java.util.HashMap;

public class LocalThicknessProcess implements TapasProcessingIJ {
    private boolean debug = false;
    private static final String LOCALTHICKNESS2D = "2D";

    HashMap<String, String> parameters;

    public LocalThicknessProcess() {
        parameters = new HashMap<>();
        setParameter(LOCALTHICKNESS2D, "no");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case LOCALTHICKNESS2D:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // get Calibration
        ImageHandler handler = ImageHandler.wrap(input);
        Calibration calibration = input.getCalibration();
        float scalex = (float) calibration.getX(1);
        float scalez = (float) calibration.getZ(1);
        float ratio = scalez / scalex;
        boolean do2D = getParameter(LOCALTHICKNESS2D).equalsIgnoreCase("yes");
        if (do2D) return localThickness2D(input);

        // 3D
        handler = handler.resample(handler.sizeX, handler.sizeY, Math.round(handler.sizeZ * ratio), ImageProcessor.NONE);
        if(debug) System.out.println("LT 0");
        //WindowManager.setTempCurrentImage(handler.getImagePlus());
        if(debug)System.out.println("LT 1");
        // then run Local thickness, LT will assume a 1 1 1 calibration
        //IJ.run("Geometry to Distance Map", "threshold=" + 128);
        ImageByte binary = handler.thresholdAboveExclusive(0);
        EDT_S1D edt_s1D = new EDT_S1D();
        edt_s1D.setup("128", binary.getImagePlus());
        if (do2D) edt_s1D.do2D = true;
        ImagePlus impDM = edt_s1D.run(null);
        if(debug) System.out.println("LT 1");
        //IJ.run("Distance Map to Distance Ridge");
        Distance_Ridge distanceRidge = new Distance_Ridge();
        distanceRidge.setup("", impDM);
        ImagePlus impDR = distanceRidge.run(null);
        if(debug) System.out.println("LT 2");
        //IJ.run("Distance Ridge to Local Thickness");
        Local_Thickness_Parallel thickness_parallel = new Local_Thickness_Parallel();
        thickness_parallel.setup("", impDR);
        thickness_parallel.run(null);
        if(debug) System.out.println("LT 3");
        //IJ.run("Local Thickness to Cleaned-Up Local Thickness");
        Clean_Up_Local_Thickness clean = new Clean_Up_Local_Thickness();
        clean.setup("", impDR);
        ImagePlus impLTC = clean.run(null);
        if(debug) System.out.println("LT 4");
        // then divide if 3D
        ImageStack stack = impLTC.getStack();
        for (int s = 1; s <= impLTC.getNSlices(); s++) {
            ImageProcessor processor = stack.getProcessor(s);
            processor.multiply(scalex);
        }
        // then go back to original calibration
        ImageHandler LTimg = ImageHandler.wrap(impLTC);
        LTimg = LTimg.resample(handler.sizeX, handler.sizeY, Math.round(handler.sizeZ / ratio), ImageProcessor.BICUBIC);
        // should be ok now
        LTimg.setScale(handler);

        return LTimg.getImagePlus();
    }

    private ImagePlus localThickness2D(ImagePlus plus) {
        ImageStack stack = plus.getStack();
        int nSlices = stack.getSize();
        ImageStack stackLT = new ImageStack(stack.getWidth(), stack.getHeight());
        for (int s = 1; s <= nSlices; s++) {
            ImageProcessor processor = stack.getProcessor(s);
            stackLT.addSlice(localThickness(processor));
        }

        return new ImagePlus("LT", stackLT);
    }

    private ImageProcessor localThickness(ImageProcessor processor) {
        ImagePlus plus = new ImagePlus("lt", processor);
        EDT_S1D edt_s1D = new EDT_S1D();
        edt_s1D.setup("128", plus);
        ImagePlus impDM = edt_s1D.run(null);
        //IJ.run("Distance Map to Distance Ridge");
        Distance_Ridge distanceRidge = new Distance_Ridge();
        distanceRidge.setup("", impDM);
        ImagePlus impDR = distanceRidge.run(null);
        //IJ.run("Distance Ridge to Local Thickness");
        Local_Thickness_Parallel thickness_parallel = new Local_Thickness_Parallel();
        thickness_parallel.setup("", impDR);
        thickness_parallel.run(null);
        //IJ.run("Local Thickness to Cleaned-Up Local Thickness");
        Clean_Up_Local_Thickness clean = new Clean_Up_Local_Thickness();
        clean.setup("", impDR);
        ImagePlus impLTC = clean.run(null);

        return impLTC.getProcessor();
    }

    @Override
    public String getName() {
        return "Local Thickness";
    }

    @Override
    public String[] getParameters() {
        return new String[]{LOCALTHICKNESS2D};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
    }
}
