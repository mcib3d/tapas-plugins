package mcib3d.tapas.IJ.plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureCompactness;
import mcib3d.geom2.measurements.MeasureEllipsoid;
import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.geom2.measurements.MeasureVolume;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;

import java.util.HashMap;

public class FilterObjectsProcess implements TapasProcessingIJ {
    private static final String MIN = "minValue";
    private static final String MAX = "maxValue";
    private static final String DESCRIPTOR = "descriptor";

    HashMap<String, String> parameters;

    public FilterObjectsProcess() {
        parameters = new HashMap<>();
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case MIN:
                parameters.put(id, value);
                return true;
            case MAX:
                parameters.put(id, value);
                return true;
            case DESCRIPTOR:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // measurements
        String descriptor = parameters.get(DESCRIPTOR);
        double min = Double.parseDouble(parameters.get(MIN));
        double max = Double.parseDouble(parameters.get(MAX));
        // population
        ImageHandler handler = ImageHandler.wrap(input.duplicate());
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        if(population.getNbObjects() ==0) {
            IJ.log("No objects to measure");
            return TapasBatchProcess.duplicatePlus(input);
        }
        int del = 0;
        for (Object3DInt object3D : population.getObjects3DInt()) {
            Double value = getMeasurement(object3D, descriptor);
            if ((Double.isNaN(value)) || (value < min) || (value > max)) {
                object3D.drawObject(handler, 0);
                del++;
            }

        }
        IJ.log(del + " objects removed using filter on " + descriptor + " between " + min + " and " + max);

        return handler.getImagePlus();
    }

    private double getMeasurement(Object3DInt object3D, String descriptor) {
        MeasureObject measureObject = new MeasureObject(object3D);
        double value = Double.NaN;
        switch (descriptor) {
            case "volume":
                value = measureObject.measure(MeasureVolume.VOLUME_UNIT);
                break;
            case "compactness":
                value = measureObject.measure(MeasureCompactness.COMP_UNIT);
                break;
            case "elongation":
                value = measureObject.measure(MeasureEllipsoid.ELL_ELONGATION);
                break;
            case "compactnessDiscrete":
                value = measureObject.measure(MeasureCompactness.COMP_DISCRETE);
                break;
        }

        return value;
    }


    @Override
    public String getName() {
        return "Remove objects based on some descriptor (outside specified range)";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DESCRIPTOR, MIN, MAX};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
    }
}
