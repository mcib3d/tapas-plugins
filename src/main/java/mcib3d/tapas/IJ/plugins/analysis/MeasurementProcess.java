package mcib3d.tapas.IJ.plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.*;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MeasurementProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";
    private static final String LIST_MEASUREMENTS = "list";

    HashMap<String, String> parameters;
    ImageInfo info;

    public MeasurementProcess() {
        parameters = new HashMap<>();
        setParameter(DIR, "?ij?");
        setParameter(FILE, "?image?-measurements.csv");
        setParameter(LIST_MEASUREMENTS, "volume");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
            case LIST_MEASUREMENTS:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        String name = parameters.get(FILE);
        String dir = parameters.get(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        // measurements
        Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(input));
        if(population.getNbObjects() ==0) {
            IJ.log("No objects to measure");
            return TapasBatchProcess.duplicatePlus(input);
        }
        List<String> list = new ArrayList<>();
        list.add("label");
        // case all
        String measList = parameters.get(LIST_MEASUREMENTS);
        if (measList.equalsIgnoreCase("all")) {
            measList = "volume,area,compactness,ellipsoid,dc,centroid";
        }
        String[] meas = measList.split(",");
        for (int i = 0; i < meas.length; i++) list.add(meas[i].trim().toLowerCase());
        /*results table and lsit of measurements
        ResultsTable resultsTable = ResultsTable.getResultsTable();
        if (resultsTable == null) resultsTable = new ResultsTable();
        else resultsTable.reset();
        final ResultsTable rt2 = resultsTable;
         */
        // buffered writer
        try (BufferedWriter bf = new BufferedWriter(new FileWriter(dir2 + name2))) {
            IJ.log("Saving measurements to " + dir2 + " " + name2);

            Map<String, Double> m = doMeasurement(population.getFirstObject(), list);
            bf.write("imageLabel");
            for (String mes : m.keySet()) {
                bf.write("," + mes);
            }
            for (Object3DInt O : population.getObjects3DInt()) {
                m = doMeasurement(O, list);
                bf.newLine();
                bf.write(input.getTitle());
                for (String mes : m.keySet()) {
                    bf.write("," + m.get(mes));
                }
                bf.flush();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // buffered writer

        /*measure objects
        AtomicInteger ai = new AtomicInteger(0);
        population.getObjects3DInt().forEach(O -> {
            rt2.incrementCounter();
            Map<String, Double> m = doMeasurement(O, list);
            for (String val : m.keySet()) rt2.setValue(val, ai.get(), m.get(val));
            ai.getAndIncrement();
        });

        IJ.log("Saving measurements to " + dir2 + " " + name2);
        try {
            resultsTable.saveAs(dir2 + name2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        return TapasBatchProcess.duplicatePlus(input);
    }

    private Map<String, Double> doMeasurement(Object3DInt object3D, List<String> list) {
        Map<String, Double> map = new HashMap<>();
        MeasureObject measureObject = new MeasureObject(object3D);
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i).trim()) {
                case "label":
                    map.put("Label", (double) object3D.getLabel());
                    break;
                case "volume":
                    map.put("Volume_Pix", measureObject.measure(MeasureVolume.VOLUME_PIX));
                    map.put("Volume_Unit", measureObject.measure(MeasureVolume.VOLUME_UNIT));
                    break;
                case "area":
                    map.put("Surface_Pix", measureObject.measure(MeasureSurface.SURFACE_PIX));
                    map.put("Surface_Unit", measureObject.measure(MeasureSurface.SURFACE_UNIT));
                    break;
                case "centroid":
                    map.put("Cx_Pix", measureObject.measure(MeasureCentroid.CX_PIX));
                    map.put("Cy_Pix", measureObject.measure(MeasureCentroid.CY_PIX));
                    map.put("Cz_Pix", measureObject.measure(MeasureCentroid.CZ_PIX));
                    break;
                case "compactness":
                    map.put("Compactness_Pix", measureObject.measure(MeasureCompactness.COMP_PIX));
                    map.put("Compactness_Unit", measureObject.measure(MeasureCompactness.COMP_UNIT));
                    map.put("Sphericity_Pix", measureObject.measure(MeasureCompactness.SPHER_PIX));
                    map.put("Sphericity_Unit", measureObject.measure(MeasureCompactness.SPHER_UNIT));
                    map.put("CompactnessDiscrete", measureObject.measure(MeasureCompactness.COMP_DISCRETE));
                    break;
                case "ellipsoid":
                    map.put("MainElongation", measureObject.measure(MeasureEllipsoid.ELL_ELONGATION));
                    map.put("MedianElongation", measureObject.measure(MeasureEllipsoid.ELL_FLATNESS));
                    map.put("RatioVolEll", measureObject.measure(MeasureEllipsoid.ELL_SPARENESS));
                    break;
                case "dc":
                    map.put("DCmean", measureObject.measure(MeasureDistancesCenter.DIST_CENTER_AVG_UNIT));
                    map.put("DCsigma", measureObject.measure(MeasureDistancesCenter.DIST_CENTER_SD_UNIT));
                    map.put("DCmin", measureObject.measure(MeasureDistancesCenter.DIST_CENTER_MIN_UNIT));
                    map.put("DCmax", measureObject.measure(MeasureDistancesCenter.DIST_CENTER_MAX_UNIT));
                    break;
            }
        }

        return map;
    }


    @Override
    public String getName() {
        return "Geometrical and shape measurements";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE, LIST_MEASUREMENTS};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
