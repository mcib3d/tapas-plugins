package mcib3d.tapas.IJ.plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class CountingProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";
    private boolean debug = true;

    HashMap<String, String> parameters;
    ImageInfo info;

    public CountingProcess() {
        parameters = new HashMap<>();
        setParameter(DIR, "?ij?");
    }

    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    public ImagePlus execute(ImagePlus input) {
        if (debug) IJ.log("Opening labelled image");
        String name = parameters.get(FILE);
        String dir = parameters.get(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        // measurements
        if (debug) IJ.log("Building population");
        ImageHandler handler = ImageHandler.wrap(input);
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        saveResults(population, input, dir2 + name2);

        return TapasBatchProcess.duplicatePlus(input);
    }


    public String getName() {
        return "Counting objects inside image";
    }

    public String[] getParameters() {
        return new String[]{DIR, FILE};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }

    private boolean saveResults(Objects3DIntPopulation population, ImagePlus input, String path) {
        double vol = getVolObjects(population);
        double imageSize = input.getWidth() * input.getHeight() * input.getNSlices();
        // buffered writer
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(path));
            bf.write("imageLabel,nbObjects,volObjects,percObjects");
            bf.newLine();
            bf.write(input.getTitle() + "," + population.getNbObjects() + "," + vol + "," + vol / imageSize);
            bf.flush();
            bf.close();
        } catch (IOException e) {
            return false;
        }
        // buffered writer

        return true;
    }

    private double getVolObjects(Objects3DIntPopulation population) {
        double vol = 0;
        return population.getObjects3DInt().stream().map(Object3DInt::size).reduce(0.0, Double::sum);
    }

}
