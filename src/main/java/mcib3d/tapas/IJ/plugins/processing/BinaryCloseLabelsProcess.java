package mcib3d.tapas.IJ.plugins.processing;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.BinaryMultiLabel;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.HashMap;

public class BinaryCloseLabelsProcess implements TapasProcessingIJ {
    public final static String RADIUSXY = "radxy";
    public final static String RADIUSZ = "radz";
    public final static String OPERATION = "operation";

    HashMap<String, String> parameters;

    public BinaryCloseLabelsProcess() {
        parameters = new HashMap<>();
        setParameter(RADIUSXY, "5");
        setParameter(RADIUSZ, "0");
        setParameter(OPERATION, "close");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case RADIUSXY:
            case RADIUSZ:
            case OPERATION:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        // get parameters
        float rx = getParameterFloat(RADIUSXY);
        float rz = getParameterFloat(RADIUSZ);
        // convert imagePlus to imageHandler then do closeLabels
        ImageHandler handler = ImageHandler.wrap(imagePlus);
        ImageHandler result;
        if ("open".equalsIgnoreCase(getParameter(OPERATION).trim())) {
            IJ.log("Performing Opening with radii " + rx + "-" + rz);
            result = BinaryMultiLabel.binaryOpenMultilabel(handler, rx, rz);
            IJ.log("DONE");
        } else {
            IJ.log("Performing Closing with radii " + rx + "-" + rz);
            result = BinaryMultiLabel.binaryCloseMultilabel(handler, rx, rz);
            IJ.log("DONE");
        }

        return result.getImagePlus();
    }

    @Override
    public String getName() {
        return "Binary morphological operation for labelled images (open, close)";
    }

    @Override
    public String[] getParameters() {
        return new String[]{RADIUSXY, RADIUSZ, OPERATION};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    private float getParameterFloat(String id) {
        return Float.parseFloat(getParameter(id));
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
    }
}
