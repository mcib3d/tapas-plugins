package mcib3d.tapas.IJ.plugins.processing;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchUtils;

import java.util.HashMap;

public class MaskProcess implements TapasProcessingIJ {
    private static final String DIR = "dirMask";
    private static final String FILE = "fileMask";

    HashMap<String, String> parameters;
    ImageInfo info;

    public MaskProcess() {
        parameters = new HashMap<>();
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        // open raw image to perform quantification
        String nameF = TapasBatchUtils.analyseFileName(parameters.get(FILE), info);
        String dirF = TapasBatchUtils.analyseDirName(parameters.get(DIR));
        IJ.log("Opening " + dirF + nameF);
        ImagePlus maskPlus = IJ.openImage(dirF + nameF);
        if (maskPlus == null) {
            IJ.log("Problem opening " + dirF + "/" + nameF);
            return imagePlus.duplicate();
        }
        ImageHandler maskHandler = ImageHandler.wrap(maskPlus);
        ImageHandler handler = ImageHandler.wrap(imagePlus);
        handler.intersectMask(maskHandler);

        return handler.getImagePlus();
    }

    @Override
    public String getName() {
        return "Apply a mask image";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
