package mcib3d.tapas.IJ.plugins.processing;

import fr.igred.omero.repository.ImageWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.OmeroConnect2;
import mcib3d.tapas.core.TapasBatchUtils;

import java.util.HashMap;
import java.util.List;

public class MaskRoiProcess implements TapasProcessingIJ {
    private static final String PROJECT = "project";
    private static final String DATASET = "dataset";
    private static final String IMAGE = "image";

    HashMap<String, String> parameters;
    ImageInfo info;

    public MaskRoiProcess() {
        parameters = new HashMap<>();
        setParameter(PROJECT, "?project?");
        setParameter(DATASET, "?dataset?");
        setParameter(IMAGE, "?image?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case PROJECT:
                parameters.put(id, value);
                return true;
            case DATASET:
                parameters.put(id, value);
                return true;
            case IMAGE:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        // get Roi
        String project = TapasBatchUtils.analyseFileName(getParameter(PROJECT), info);
        String dataset = TapasBatchUtils.analyseFileName(getParameter(DATASET), info);
        String image = TapasBatchUtils.analyseFileName(getParameter(IMAGE), info);

        OmeroConnect2 omeroConnect2 = new OmeroConnect2();
        ImageWrapper imageWrapper = omeroConnect2.findOneImage(project, dataset, image, true);
        List<Roi> rois = omeroConnect2.getImageRois(imageWrapper);
        IJ.log("Found " + rois.size() + " rois");

        ImageHandler imageHandler = ImageHandler.wrap(imagePlus);
        rois.forEach(roi -> clearOutsideRoi(imageHandler, roi));

        return imageHandler.getImagePlus();
    }

    private void clearOutsideRoi(ImageHandler imageHandler, Roi roi) {
        if (!roi.isArea()) return; // roi not closed, return
        for (int y = 0; y < imageHandler.sizeY; y++) {
            for (int x = 0; x < imageHandler.sizeX; x++) {
                if (!roi.contains(x, y)) {
                    for (int z = 0; z < imageHandler.sizeZ; z++) {
                        imageHandler.setPixel(x, y, z, 0);
                    }
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Clear outside rois";
    }

    @Override
    public String[] getParameters() {
        return new String[]{PROJECT, DATASET, IMAGE};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
