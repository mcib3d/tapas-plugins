package mcib3d.tapas.IJ.plugins.processing;

import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.regionGrowing.Watershed3DVoronoi;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.HashMap;

public class WatershedVoronoiProcess implements TapasProcessingIJ {
    private static final String RADIUS = "radius";
    HashMap<String, String> parameters;
    ImageInfo info;

    public WatershedVoronoiProcess() {
        parameters = new HashMap<>();
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case RADIUS:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        int radius = Integer.parseInt(getParameter(RADIUS));
        ImageHandler image = ImageHandler.wrap(imagePlus);
        Watershed3DVoronoi watershed3DVoronoi = new Watershed3DVoronoi(image, radius);
        ImageHandler zones = watershed3DVoronoi.getVoronoiZones(false);

        return zones.getImagePlus();
    }

    @Override
    public String getName() {
        return "Voronoi zones from labelled images, with extension radius";
    }

    @Override
    public String[] getParameters() {
        return new String[]{RADIUS};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
