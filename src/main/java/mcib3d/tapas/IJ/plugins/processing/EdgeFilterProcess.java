package mcib3d.tapas.IJ.plugins.processing;

import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.CannyEdge3D;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.HashMap;

public class EdgeFilterProcess implements TapasProcessingIJ {
    public final static String ALPHA = "alpha";

    HashMap<String, String> parameters;

    public EdgeFilterProcess() {
        parameters = new HashMap<>();
        setParameter(ALPHA, "0.5");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id){
            case ALPHA:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        ImageHandler handler = ImageHandler.wrap(imagePlus);
        CannyEdge3D edges = new CannyEdge3D(handler, Double.parseDouble(getParameter(ALPHA)));

        return edges.getEdge().getImagePlus();
    }

    @Override
    public String getName() {
        return "Compute edges (Canny)";
    }

    @Override
    public String[] getParameters() {
        return new String[]{ALPHA};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
    }
}
