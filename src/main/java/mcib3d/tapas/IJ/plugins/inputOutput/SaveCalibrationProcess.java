package mcib3d.tapas.IJ.plugins.inputOutput;

import ij.IJ;
import ij.ImagePlus;
import ij.measure.Calibration;
import mcib3d.tapas.IJ.TapasProcessingIJ;

import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SaveCalibrationProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE = "file";

    Map<String, String> parameters;
    ImageInfo info;

    public SaveCalibrationProcess() {
        parameters = new HashMap<>();
        setParameter(DIR, "?ij?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // get the file
        String dir = TapasBatchUtils.analyseDirName(getParameter(DIR));
        String file = TapasBatchUtils.analyseFileName(getParameter(FILE), info);
        try {
            // get calibration
            Calibration calibration = input.getCalibration();
            double sxy = 1;
            double sz = 1;
            String unit = "um";
            if (calibration != null) {
                sxy = calibration.getX(1);
                sz = calibration.getZ(1);
                unit = calibration.getUnit();
            }
            // save file
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat) nf;
            formatter.applyPattern("#########.#########");
            BufferedWriter bw = new BufferedWriter(new FileWriter(dir + file));
            if (file.equalsIgnoreCase("pixel_size_in_micrometer.txt")) // for axonDeepSeg
                bw.write("" + formatter.format(sxy));
            else
                bw.write(sxy + ":" + sz + ":" + unit);
            bw.close();
        } catch (IOException e) {
            IJ.log("Pb with file " + dir + file + " : " + e.getMessage());
        }

        return TapasBatchProcess.duplicatePlus(input);
    }

    @Override
    public String getName() {
        return "Save the scale calibration  properties";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
