package mcib3d.tapas.IJ.plugins.inputOutput;

import ij.ImagePlus;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.util.HashMap;

public class AddTagProcess implements TapasProcessingIJ {
    private static final String PROJECT = "project";
    private static final String DATASET = "dataset";
    private static final String IMAGE = "image";
    private static final String TAGS = "tags";

    HashMap<String, String> parameters;
    ImageInfo info;

    public AddTagProcess() {
        parameters = new HashMap<>();
        setParameter(PROJECT, "?project?");
        setParameter(DATASET, "?dataset?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case PROJECT:
            case DATASET:
            case "name": // deprecated
            case IMAGE:
            case TAGS:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        // update final name
        String name = getParameter(IMAGE);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String project = getParameter(PROJECT);
        String project2 = TapasBatchUtils.analyseFileName(project, info);
        String dataset = getParameter(DATASET);
        String dataset2 = TapasBatchUtils.analyseFileName(dataset, info);
        String[] tags = getParameter(TAGS).split(",");

        TapasBatchProcess.addTags(project2, dataset2, name2, tags);

        return imagePlus.duplicate();
    }

    @Override
    public String getName() {
        return "Add tags to an image (OMERO).";
    }

    @Override
    public String[] getParameters() {
        return new String[]{PROJECT, DATASET, IMAGE, TAGS};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
