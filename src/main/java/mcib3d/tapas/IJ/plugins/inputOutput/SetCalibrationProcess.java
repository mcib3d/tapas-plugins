package mcib3d.tapas.IJ.plugins.inputOutput;

import fr.igred.omero.repository.ImageWrapper;
import ij.IJ;
import ij.ImagePlus;
import ij.measure.Calibration;
import mcib3d.tapas.IJ.TapasProcessingIJ;

import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.OmeroConnect;
import mcib3d.tapas.core.TapasBatchProcess;

import java.util.HashMap;

public class SetCalibrationProcess implements TapasProcessingIJ {
    final static private String SCALEXY = "scaleXY";
    final static private String SCALEZ = "scaleZ";
    final static private String UNIT = "unit";

    ImageInfo info;
    HashMap<String, String> parameters;

    public SetCalibrationProcess() {
        parameters = new HashMap<>();
        setParameter(SCALEXY, "1");
        setParameter(SCALEZ, "1");
        setParameter(UNIT, "um");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case SCALEXY: // test value
                parameters.put(id, value);
                return true;
            case SCALEZ:
                parameters.put(id, value);
                return true;
            case UNIT:
                if (value.equalsIgnoreCase("um") || value.equalsIgnoreCase("mm")) {
                    parameters.put(id, value);
                    return true;
                } else {
                    IJ.log("Only unit um or mm are accepted.");
                }
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        double sxy = Double.parseDouble(getParameter(SCALEXY).trim());
        double sz = Double.parseDouble(getParameter(SCALEZ).trim());
        String unit = getParameter(UNIT).trim();
        if (info.isOmero()) {
            try {
                OmeroConnect connect = new OmeroConnect();
                ImageWrapper imageData = connect.findOneImage(info);
                if (imageData != null) {
                    if (connect.setResolutionImage(imageData, sxy, sz, unit)) {
                        IJ.log("Set resolution to " + sxy + " " + sz+ " "+unit);
                    } else {
                        IJ.log("Pb to set resolution");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // change resolution for inout
        Calibration calibration = input.getCalibration();
        if (calibration == null) {
            calibration = new Calibration();
        }
        calibration.setUnit(unit);
        calibration.pixelWidth = sxy;
        calibration.pixelHeight = sxy;
        calibration.pixelDepth = sz;

        input.setCalibration(calibration);

        return TapasBatchProcess.duplicatePlus(input);
    }

    @Override
    public String getName() {
        return "Set the image scale, update scale if OMERO, in um or mm";
    }

    @Override
    public String[] getParameters() {
        return new String[]{SCALEXY, SCALEZ, UNIT};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
