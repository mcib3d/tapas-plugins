package mcib3d.tapas.IJ.plugins.inputOutput;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.util.HashMap;

public class LinkProcess implements TapasProcessingIJ {
    private static final String PROJECT = "project";
    private static final String DATASET = "dataset";
    private static final String IMAGE = "image";
    private static final String CHANNEL = "channel";
    private static final String FRAME = "frame";
    private static final String LINKNAME = "linkName";

    ImageInfo info;
    HashMap<String, String> parameters;

    public LinkProcess() {
        info = new ImageInfo();
        parameters = new HashMap<>();
        setParameter(PROJECT, "?project?");
        setParameter(DATASET, "?dataset?");
        setParameter(IMAGE, "?image?");
        setParameter(CHANNEL, "?channel?");
        setParameter(FRAME, "?frame?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case PROJECT:
            case DATASET:
            case "name": // deprecated
            case IMAGE:
            case CHANNEL:
            case FRAME:
            case LINKNAME:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        String linkName = getParameter(LINKNAME);
        String project = TapasBatchUtils.analyseFileName(getParameter(PROJECT),info);
        String dataset = TapasBatchUtils.analyseFileName(getParameter(DATASET),info);
        String image = TapasBatchUtils.analyseFileName(getParameter(IMAGE),info);
        int channel = Integer.parseInt(TapasBatchUtils.analyseFileName(getParameter(CHANNEL),info));
        int frame = Integer.parseInt(TapasBatchUtils.analyseFileName(getParameter(FRAME),info));
        ImageInfo infoLink = new ImageInfo(project, dataset, image, channel, frame);

        IJ.log("Setting link "+linkName+" to "+infoLink);
        TapasBatchProcess.setlink(linkName, infoLink);

        return TapasBatchProcess.duplicatePlus(input);
    }

    @Override
    public String getName() {
        return "Set a OMERO link to an image";
    }

    @Override
    public String[] getParameters() {
        return new String[]{PROJECT, DATASET, IMAGE, CHANNEL, FRAME, LINKNAME};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
