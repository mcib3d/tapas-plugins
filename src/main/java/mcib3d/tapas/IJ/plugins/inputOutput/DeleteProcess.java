 package mcib3d.tapas.IJ.plugins.inputOutput;

 import ij.IJ;
 import ij.ImagePlus;
 import mcib3d.tapas.IJ.TapasProcessingIJ;
 import mcib3d.tapas.core.ImageInfo;
 import mcib3d.tapas.core.TapasBatchProcess;
 import mcib3d.tapas.core.TapasBatchUtils;

 import java.io.File;
 import java.util.HashMap;

public class DeleteProcess implements TapasProcessingIJ {
    public static final String DIR = "dir";
    public static final String FILE = "file";
    HashMap<String, String> parameters;
    ImageInfo info;

    public DeleteProcess() {
        parameters = new HashMap<>();
        setParameter(DIR,"?ij?");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        String name = getParameter(FILE);
        String dir = getParameter(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        File file = new File(dir2 + name2);
        if (file.exists()) {
            IJ.log("Deleting " + file.getPath());
            file.delete();
        }
        else {
            IJ.log("File "+ file.getPath()+" not found");
        }

        return TapasBatchProcess.duplicatePlus(input);
    }

    @Override
    public String getName() {
        return "Deleting file";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE};
    }

    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }

}
