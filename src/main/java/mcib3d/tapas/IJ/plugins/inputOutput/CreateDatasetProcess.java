package mcib3d.tapas.IJ.plugins.inputOutput;

import fr.igred.omero.Client;
import fr.igred.omero.exception.AccessException;
import fr.igred.omero.exception.ServiceException;
import fr.igred.omero.repository.DatasetWrapper;
import fr.igred.omero.repository.ProjectWrapper;
import ij.IJ;
import ij.ImagePlus;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.*;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CreateDatasetProcess implements TapasProcessingIJ {
    private static final String PROJECT = "project";
    private static final String DATASETNAME = "datasetName";
    private static final String DATASETCOMMENT = "description";

    HashMap<String, String> parameters;
    ImageInfo info;

    public CreateDatasetProcess() {
        parameters = new HashMap<>();
        setParameter(PROJECT, "?project?");
        setParameter(DATASETNAME, "myDataset");
        setParameter(DATASETCOMMENT, "Created by TAPAS");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case PROJECT:
            case DATASETNAME:
            case DATASETCOMMENT:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        String projectName = TapasBatchUtils.analyseFileName(getParameter(PROJECT), info);
        String datasetName = TapasBatchUtils.analyseFileName(getParameter(DATASETNAME), info);
        if (info.isOmero()) {
            OmeroConnect2 omeroConnect2 = new OmeroConnect2();
            ProjectWrapper projectWrapper = omeroConnect2.findProject(projectName, true);
            List<DatasetWrapper> list = projectWrapper.getDatasets(datasetName);
            if (!list.isEmpty())
                IJ.log("Dataset " + datasetName + " already exists in " + projectName + ", it will not be created.");
            else {
                try {
                    DatasetWrapper datasetWrapper = projectWrapper.addDataset(omeroConnect2.getOmeroClient(), datasetName, getParameter(DATASETCOMMENT).trim());
                    IJ.log("Dataset " + datasetWrapper.getName() + " created in " + projectWrapper.getName() + " (" + datasetWrapper.getDescription() + ")");
                } catch (ServiceException | AccessException | ExecutionException e) {
                    System.out.println("Create Dataset Error " + e);
                }
            }
        }
        // files
        else {
            File dirNew = new File(info.getRootDir() + File.separator + info.getProject() + File.separator + datasetName);
            System.out.println("New dataset " + dirNew.getPath());
            if (dirNew.exists())
                IJ.log("Dataset " + datasetName + " already exists in " + projectName + ", it will not be created.");
            else dirNew.mkdir();
        }
        
        return TapasBatchProcess.duplicatePlus(imagePlus);
    }

    @Override
    public String getName() {
        return "Create dataset.";
    }

    @Override
    public String[] getParameters() {
        return new String[]{PROJECT, DATASETNAME, DATASETCOMMENT};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
