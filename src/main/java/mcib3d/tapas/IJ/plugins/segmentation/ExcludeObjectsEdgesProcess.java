package mcib3d.tapas.IJ.plugins.segmentation;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.Objects3DIntPopulationComputation;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.HashMap;

public class ExcludeObjectsEdgesProcess implements TapasProcessingIJ {
    private static final String TOUCHZ = "excludeZ";

    HashMap<String, String> parameters;

    public ExcludeObjectsEdgesProcess() {
        parameters = new HashMap<>();
        parameters.put(TOUCHZ, "no");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case TOUCHZ:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // touchZ
        boolean touchZ = getParameter(TOUCHZ).equalsIgnoreCase("yes");
        // population
        ImageHandler handler = ImageHandler.wrap(input.duplicate());
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        Objects3DIntPopulation population1 = new Objects3DIntPopulationComputation(population).getExcludeBorders(handler, touchZ);
        // image for results
        ImageHandler handler1;
        if (input.getBitDepth() > 16) {
            handler1 = new ImageFloat("Objects_removed", input.getWidth(), input.getHeight(), input.getNSlices());
        } else {
            handler1 = new ImageShort("Objects_removed", input.getWidth(), input.getHeight(), input.getNSlices());
        }
        population1.drawInImage(handler1);

        IJ.log(""+(population.getNbObjects()-population1.getNbObjects())+" objects removed");

        return handler1.getImagePlus();
    }

    @Override
    public String getName() {
        return "Delete objects touching edges";
    }

    @Override
    public String[] getParameters() {
        return new String[]{TOUCHZ};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
    }
}
