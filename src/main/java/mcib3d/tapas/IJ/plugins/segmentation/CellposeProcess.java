package mcib3d.tapas.IJ.plugins.segmentation;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.HashMap;

public class CellposeProcess implements TapasProcessingIJ {
    private static final String PYTHON = "python";
    private static final String DIR = "directory";
    private static final String SPLITXY = "splitXY";
    private static final String OVERLAPXY = "overlapXY";
    private static final String MODEL = "model";
    private static final String DIAMETER = "diameter";
    private static final String FLOWTHRESHOLD = "flow_threshold";
    private static final String CELLTHRESHOLD = "cellprob_threshold";
    private static final String DO3D = "3d";
    private static final String STITCH3D = "stitch3d";


    HashMap<String, String> parameters;

    public CellposeProcess() {
        parameters = new HashMap<>();
        setParameter(PYTHON, "/home/thomas/App/miniconda3/envs/cellpose/bin/python");
        setParameter(DIR, "?home?/tmpcellpose");
        setParameter(SPLITXY, "1");
        setParameter(OVERLAPXY, "0");
        setParameter(MODEL, "nuclei");
        setParameter(DIAMETER, "30.0");
        setParameter(DO3D, "yes");
        setParameter(FLOWTHRESHOLD, "0.4");
        setParameter(CELLTHRESHOLD, "0.0");
        setParameter(STITCH3D, "0.5");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case PYTHON:
            case DIR:
            case OVERLAPXY:
            case SPLITXY:
            case MODEL:
            case DIAMETER:
            case DO3D:
            case FLOWTHRESHOLD:
            case STITCH3D:
            case CELLTHRESHOLD:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        ImageHandler img = ImageHandler.wrap(imagePlus);
        int splitXY = Integer.parseInt(getParameter(SPLITXY).trim());
        int overlapXY = Integer.parseInt(getParameter(OVERLAPXY).trim());
        Cellpose cellpose = new Cellpose();
        cellpose.setPython(getParameter(PYTHON));
        cellpose.setDirectory(getParameter(DIR));
        cellpose.setDiameter(Double.parseDouble(getParameter(DIAMETER).trim()));
        cellpose.setFlowThreshold(Double.parseDouble(getParameter(FLOWTHRESHOLD).trim()));
        cellpose.setCellThreshold(Double.parseDouble(getParameter(CELLTHRESHOLD).trim()));
        cellpose.setModel(getParameter(MODEL).trim());
        cellpose.setDo3d(getParameter(DO3D).trim().equalsIgnoreCase("yes"));
        ImageHandler seg = cellpose.segmentSplitImage(img, splitXY, overlapXY);

        return seg.getImagePlus();
    }

    @Override
    public String getName() {
        return "Cellpose, with split options (only one channel)";
    }

    @Override
    public String[] getParameters() {
        return new String[]{PYTHON, DIR, SPLITXY, OVERLAPXY, MODEL, DIAMETER, FLOWTHRESHOLD, CELLTHRESHOLD, STITCH3D, DO3D};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
    }

    public static void main(String[] args) {
        ImageHandler img1 = ImageHandler.wrap(IJ.openImage("/home/thomas/tmpcellpose/image.tif"));

        new CellposeProcess().execute(img1.getImagePlus()).show();
    }
}
