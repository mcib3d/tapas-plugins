package mcib3d.tapas.IJ.plugins.segmentation;

import ij.ImagePlus;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.image3d.segment.*;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.utils.Thresholder;

import java.util.HashMap;

public class SpotsSegProcess implements TapasProcessingIJ {
    final static private String MIN_VOLUME = "minVolume";
    final static private String MAX_VOLUME = "maxVolume";
    final static private String SEEDS_THRESHOLD = "seedsThreshold";
    final static private String SEEDS_RADIUS = "seedsRadius";
    private HashMap<String, String> parameters;

    public SpotsSegProcess() {
        parameters = new HashMap<>();
        setParameter(MIN_VOLUME, "1");
        setParameter(MAX_VOLUME, "1000000");
        setParameter(SEEDS_RADIUS, "2");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case MIN_VOLUME:
                parameters.put(id, value);
                return true;
            case MAX_VOLUME:
                parameters.put(id, value);
                return true;
            case SEEDS_THRESHOLD:
                parameters.put(id, value);
                return true;
            case SEEDS_RADIUS:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // compute seeds
        ImageHandler handler = ImageHandler.wrap(input);
        float radius = Float.parseFloat(parameters.get(SEEDS_RADIUS));
        ImageHandler seeds = FastFilters3D.filterImage(handler, FastFilters3D.MAXLOCAL, radius, radius, radius, 0, false);
        // Threshold
        float thmin = 0;
        String thresholdS = parameters.get(SEEDS_THRESHOLD);
        // Test %
        thmin = Thresholder.getThreshold(ImageHandler.wrap(input), thresholdS);
        // segmentation
        Segment3DSpots seg = new Segment3DSpots(handler, seeds);
        // set parameters
        seg.setSeedsThreshold((int) thmin);
        seg.setVolumeMin(Integer.parseInt(parameters.get(MIN_VOLUME)));
        seg.setVolumeMax(Integer.parseInt(parameters.get(MAX_VOLUME)));
        // create thresholder and segmenter
        LocalThresholder localThresholder = new LocalThresholderGaussFit(10, 1.5);
        SpotSegmenter spotSegmenter = new SpotSegmenterMax();
        // segmentation
        seg.setLocalThresholder(localThresholder);
        seg.setSpotSegmenter(spotSegmenter);
        seg.segmentAll();

        return seg.getLabeledImage().getImagePlus();
    }

    @Override
    public String getName() {
        return "Spots segmentation";
    }

    @Override
    public String[] getParameters() {
        return new String[]{MIN_VOLUME, MAX_VOLUME, SEEDS_RADIUS, SEEDS_THRESHOLD};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
    }
}
