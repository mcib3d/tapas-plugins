package mcib3d.tapas.IJ.plugins.segmentation;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.IterativeThresholding2.TrackThreshold2;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchUtils;
import mcib3d.tapas.utils.Thresholder;

import java.util.HashMap;

public class IterativeThresholdingProcess implements TapasProcessingIJ {
    final static private String MIN_VOLUME = "minVolume";
    final static private String MAX_VOLUME = "maxVolume";
    final static private String TH_MIN = "minThreshold";
    final static private String TH_STEP = "stepThreshold";
    final static private String CRITERIA = "criteria";
    final static private String USERS = "user";

    ImageInfo info;
    HashMap<String, String> parameters;

    public IterativeThresholdingProcess() {
        parameters = new HashMap<>();
        setParameter(MIN_VOLUME, "100");
        setParameter(MAX_VOLUME, "-1");
        setParameter(TH_MIN, "0");
        setParameter(TH_STEP, "1");
        setParameter(CRITERIA, "ELONGATION");
        setParameter(USERS, "-");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case MIN_VOLUME:
                parameters.put(id, value);
                return true;
            case MAX_VOLUME:
                parameters.put(id, value);
                return true;
            case TH_MIN:
                parameters.put(id, value);
                return true;
            case TH_STEP:
                parameters.put(id, value);
                return true;
            case USERS:
                parameters.put(id, value);
                return true;
            case CRITERIA:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        input.getProcessor().getHistogram();
        String users = parameters.get(USERS);
        // check threshold, any key ?
        // if not value, look for key in core
        float thmin = 0;
        String thresholdS = parameters.get(TH_MIN);
        // Test %
        thmin = Thresholder.getThreshold(ImageHandler.wrap(input), thresholdS);
//        if (thresholdS.contains("%")) {
//            double percent = Double.parseDouble(thresholdS.substring(0, thresholdS.indexOf("%")).trim());
//            ImageHandler img = ImageHandler.wrap(input);
//            thmin = (int) img.getPercentile(percent / 100.0, null);
//            IJ.log("Thresholding with first value : " + thmin);
//        } else thmin = Integer.parseInt(TapasBatchUtils.getKey(thresholdS, info));
        //other parameters
        int volMin = Integer.parseInt(parameters.get(MIN_VOLUME));
        int volMax = Integer.parseInt(parameters.get(MAX_VOLUME));
        String criteriaText = parameters.get(CRITERIA);
        if (volMax < 0) volMax = Integer.MAX_VALUE;
        int minCont = 0;
        // step
        thresholdS = parameters.get(TH_STEP);
        int step = Integer.parseInt(TapasBatchUtils.getKey(thresholdS, info));

        TrackThreshold2 TT = new TrackThreshold2(volMin, volMax, minCont, step, step, thmin);
        TT.setMethodThreshold(TrackThreshold2.THRESHOLD_METHOD_STEP);
        TT.setCriteriaMethod(getCriteria(criteriaText));
        ImagePlus res = TT.segmentBest(input, true);
        if (res == null) {
            // no objects found, create blank image
            IJ.log("No objects found.");
            res = input.duplicate();
            ImageStack stack = res.getImageStack();
            for (int s = 1; s <= res.getNSlices(); s++)
                stack.getProcessor(s).set(0);

        }
        if (input.getCalibration() != null) res.setCalibration(input.getCalibration());

        return res;
    }

    private int getCriteria(String text) {
        switch (text.toUpperCase()) {
            case "COMPACTNESS":
                return TrackThreshold2.CRITERIA_METHOD_MAX_COMPACTNESS;
            case "VOLUME":
                return TrackThreshold2.CRITERIA_METHOD_MAX_VOLUME;
            case "MSER":
                return TrackThreshold2.CRITERIA_METHOD_MSER;
            case "EDGES":
                return TrackThreshold2.CRITERIA_METHOD_MAX_EDGES;
            default:
                return TrackThreshold2.CRITERIA_METHOD_MIN_ELONGATION;
        }
    }

    @Override
    public String getName() {
        return "Iterative thresholding";
    }

    @Override
    public String[] getParameters() {
        return new String[]{MIN_VOLUME, MAX_VOLUME, TH_MIN, CRITERIA};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
        info = currentImage;
    }
}
