package mcib3d.tapas.IJ.plugins.segmentation;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import net.haesleinhuepf.clij.CLIJ;
import net.haesleinhuepf.clij.clearcl.ClearCLBuffer;
import net.haesleinhuepf.clijx.CLIJx;
import net.haesleinhuepf.clijx.plugins.*;

import java.util.HashMap;

public class CLIJLocalThresholdProcess implements TapasProcessingIJ {
    public final static String RADIUS = "radius";
    public final static String PAR1 = "par1";
    public final static String PAR2 = "par2";
    public final static String FILTER = "method";
    HashMap<String, String> parameters;

    public CLIJLocalThresholdProcess() {
        // check install
        ClassLoader loader = IJ.getClassLoader();
        try {
            loader.loadClass("net.haesleinhuepf.clijx.CLIJx");
        } catch (Exception e) {
            IJ.log("CLIJx not installed, please install from update site");
            return;
        }
        parameters = new HashMap<>();
        setParameter(RADIUS, "15");
        setParameter(PAR1, "15");
        setParameter(PAR1, "0");
        setParameter(FILTER, "Bernsen");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case RADIUS:
                parameters.put(id, value);
                return true;
            case PAR1:
                parameters.put(id, value);
                return true;
            case PAR2:
                parameters.put(id, value);
                return true;
            case FILTER:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        // get parameters
        float radius = Float.parseFloat(getParameter(RADIUS));
        float par1 = Float.parseFloat(getParameter(PAR1));
        float par2 = Float.parseFloat(getParameter(PAR2));
        String filterS = getParameter(FILTER).toLowerCase();

        IJ.log("GPU Local Thresholding with filter " + filterS + " and radius " + radius);
        // init CLIJ and create images
        CLIJ clij = CLIJ.getInstance();
        ClearCLBuffer inputCLBuffer = clij.push(input);

        // get result
        ClearCLBuffer outputCLBuffer;
        outputCLBuffer = localThreshold(clij, inputCLBuffer, filterS, radius, par1, par2);
        ImagePlus dstImagePlus = clij.pull(outputCLBuffer);
        dstImagePlus.setCalibration(input.getCalibration());
        // cleanup memory on GPU
        inputCLBuffer.close();
        outputCLBuffer.close();

        return dstImagePlus;
    }


    private ClearCLBuffer localThreshold(CLIJ clij, ClearCLBuffer inputCLBuffer, String filter, float radius, float par1, float par2) {
        ClearCLBuffer outputCLBuffer = clij.create(inputCLBuffer);
        CLIJx cliJx = CLIJx.getInstance();

        switch (filter) {
            case "bernsen":
                LocalThresholdBernsen.localThresholdBernsen(cliJx, inputCLBuffer, outputCLBuffer, radius, par1);
                break;
            case "contrast":
                LocalThresholdContrast.localThresholdContrast(cliJx, inputCLBuffer, outputCLBuffer, radius);
                break;
            case "mean":
                LocalThresholdMean.localThresholdMean(cliJx,inputCLBuffer,outputCLBuffer,radius, par1);
                break;
            case "median":
                LocalThresholdMedian.localThresholdMedian(cliJx, inputCLBuffer, outputCLBuffer,radius, par1);
                break;
            case "midgrey":
                LocalThresholdMidGrey.localThresholdMidGrey(cliJx, inputCLBuffer, outputCLBuffer,radius, par1);
                break;
            case "nidblack":
                LocalThresholdNiblack.localThresholdNiblack(cliJx,inputCLBuffer,outputCLBuffer,radius,par1,par2);
                break;
            case "phansalkar":
                LocalThresholdPhansalkar.localThresholdPhansalkar(cliJx,inputCLBuffer,outputCLBuffer,radius,par1,par2);
                break;
            case "sauvola":
                LocalThresholdSauvola.localThresholdSauvola(cliJx,inputCLBuffer,outputCLBuffer,radius,par1,par2);
                break;
        }

        return outputCLBuffer;
    }

    @Override
    public String getName() {
        return "Local Thresholding with CLIJ";
    }

    @Override
    public String[] getParameters() {
        return new String[]{RADIUS, PAR1, PAR2, FILTER};
    }

    @Override
    public String getParameter(String id) {
        return parameters.get(id);
    }

    @Override
    public void setCurrentImage(ImageInfo currentImage) {
    }
}
