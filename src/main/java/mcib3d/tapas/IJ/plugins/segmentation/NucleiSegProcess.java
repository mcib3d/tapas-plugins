package mcib3d.tapas.IJ.plugins.segmentation;

import ij.ImagePlus;
import ij.process.AutoThresholder;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.segment.Segment3DNuclei;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;

import java.util.HashMap;

public class NucleiSegProcess implements TapasProcessingIJ {
    final static private String METHOD = "method";
    final static private String SEPARATE = "separate";
    final static private String MANUAL = "manual";

    ImageInfo info;
    HashMap<String, String> parameters;

    public NucleiSegProcess() {
        parameters = new HashMap<>();
        setParameter(METHOD, "Otsu");
        setParameter(SEPARATE, "yes");
        setParameter(MANUAL, "0");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case METHOD: // test value
                parameters.put(id, value);
                return true;
            case SEPARATE: // test value
                parameters.put(id, value);
                return true;
            case MANUAL:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        String method = getParameter(METHOD);
        float manual = Float.parseFloat(getParameter(MANUAL));
        boolean separate = getParameter(SEPARATE).equalsIgnoreCase("yes");
        ImageHandler imageHandler = ImageHandler.wrap(imagePlus);
        Segment3DNuclei segment3DNuclei = new Segment3DNuclei(imageHandler);
        segment3DNuclei.setMethod(AutoThresholder.Method.valueOf(method));
        segment3DNuclei.setManual(manual);
        segment3DNuclei.setSeparate(separate);
        ImageHandler seg = segment3DNuclei.segment();

        return seg.getImagePlus();
    }

    @Override
    public String getName() {
        return "3D Nuclei segmentation based on 2D projection";
    }

    @Override
    public String[] getParameters() {
        return new String[]{METHOD, MANUAL, SEPARATE};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
