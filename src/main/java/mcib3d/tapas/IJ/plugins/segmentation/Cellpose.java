package mcib3d.tapas.IJ.plugins.segmentation;

import ij.IJ;
import ij.ImagePlus;
import ij.measure.Calibration;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.processing.ImageCropper;
import mcib3d.image3d.processing.ImageStitcher;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Cellpose {
    private String python = "/home/thomas/miniconda3/envs/cellpose/bin/python"; // CHANGE PATH
    private String directory = IJ.getDirectory("temp");
    private String model = "nuclei";
    private double diameter = 30.0;

    private double flowThreshold = 0.4; // QC cellpose default is 0.4
    private double cellThreshold = 0; // Cell threshold default 0, decrease to find more and larger cells
    private double stitchThreshold = 0.5;
    private boolean do3d = true;

    public String getPython() {
        return python;
    }

    public void setPython(String python) {
        this.python = python;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public double getFlowThreshold() {
        return flowThreshold;
    }

    public void setFlowThreshold(double flowThreshold) {
        this.flowThreshold = flowThreshold;
    }

    public double getCellThreshold() {
        return cellThreshold;
    }

    public void setCellThreshold(double cellThreshold) {
        this.cellThreshold = cellThreshold;
    }

    public double getStitchThreshold() {
        return stitchThreshold;
    }

    public void setStitchThreshold(double stitchThreshold) {
        this.stitchThreshold = stitchThreshold;
    }

    public boolean isDo3d() {
        return do3d;
    }

    public void setDo3d(boolean do3d) {
        this.do3d = do3d;
    }

    public ImageHandler segmentSplitImage(ImageHandler img, int nbSplitXY, int overlapXY) {
        return segmentSplitImage(img, nbSplitXY, nbSplitXY, 1, overlapXY, overlapXY, 0);
    }

    public ImageHandler segmentSplitImage(ImageHandler img, int nbSplitX, int nbSplitY, int nbSplitZ, int overlapX, int overlapY, int overlapZ) {
        int sizeX = (img.sizeX + (nbSplitX - 1) * overlapX) / nbSplitX;
        int sizeY = (img.sizeY + (nbSplitY - 1) * overlapY) / nbSplitY;
        int sizeZ = (img.sizeZ + (nbSplitZ - 1) * overlapZ) / nbSplitZ;

        // split and save calibrations
        System.out.println("SPLITTING");
        List<ImageHandler> files = split(img, sizeX, sizeY, sizeZ, overlapX, overlapY, overlapZ);
        //saveCalibration(files, directory);
        // segment images
        System.out.println("SEGMENTING");
        List<ImageHandler> segs = files.stream().map(crop -> segmentOneImage(crop, directory)).collect(Collectors.toList());
        // stitch images
        System.out.println("STITCHING");
        ImageHandler finalSeg = new ImageShort("seg_" + img.getTitle(), img.sizeX, img.sizeY, img.sizeZ);
        finalSeg.setVoxelSize(img);
        new ImageStitcher(segs).stitchLabelImages().drawInImage(finalSeg);
        // delete dir
        //Files.delete(Paths.get(directory,"cals.txt"));
        //Files.delete(Paths.get(directory));
        System.out.println("FINISHED");
        return finalSeg;

    }

    private void saveCalibration(List<ImageHandler> crops, String directory) {
        // SAVE INFORMATION
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(directory + File.separator + "cals.txt"));
            crops.forEach(crop -> {
                Calibration calibration = crop.getImagePlus().getCalibration();
                try {
                    bf.write(calibration.pixelWidth + ":" + calibration.pixelHeight + ":" + calibration.pixelDepth
                            + ":" + calibration.xOrigin + ":" + calibration.yOrigin + ":" + calibration.zOrigin + "\n");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
            bf.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private List<ImageHandler> split(ImageHandler img1, int sizeX, int sizeY, int sizeZ, int overlapX, int overlapY, int overlapZ) {
        List<ImageHandler> crops = new ImageCropper(img1).cropSplit(sizeX, sizeY, sizeZ, overlapX, overlapY, overlapZ);
        // save image
        AtomicInteger ai = new AtomicInteger(0);
        crops.forEach(crop -> crop.setTitle("crop_" + ai.getAndIncrement()));

        return crops;
    }

    public ImageHandler segmentOneImage(ImageHandler img) {
        return segmentOneImage(img, "");
    }


    public ImageHandler segmentOneImage(ImageHandler img, String directory) {
        String do3ds = do3d ? "--do_3D" : "--stitch_threshold " + stitchThreshold;
        double anisotropy = img.getVoxelSizeZ() / img.getVoxelSizeXY();
        anisotropy = Math.max(1.0, anisotropy);
        anisotropy = Math.min(4.0, anisotropy);
        Date date = new Date();
        try {
            String dirCellposeTmp;
            File dirCellpose;
            if (directory.isEmpty()) {
                dirCellposeTmp = Files.createTempDirectory("cellpose").toString();
                dirCellpose = new File(dirCellposeTmp);
            } else {
                dirCellposeTmp = directory + File.separator + "cellpose" + date.getTime();
                dirCellpose = new File(dirCellposeTmp);
                dirCellpose.mkdir();
            }
            //System.out.println("Temp cellpose directory " + dirCellposeTmp);
            String title = img.getTitle();
            // save image
            img.setTitle("image");
            img.save(dirCellposeTmp, true);
            //System.out.println("Exe python " + python);
            String cmd = python + " -m cellpose --dir " + dirCellposeTmp + " --chan 0 --pretrained_model " + model + " --diameter " + diameter
                    + " --flow_threshold " + flowThreshold + " --cellprob_threshold " + cellThreshold
                    + " --save_tif --no_npy " + do3ds + " --use_gpu --anisotropy " + anisotropy + " --savedir " + dirCellposeTmp;
            System.out.println(cmd);

            // EXE from BIOP cellpose wrapper
            ProcessBuilder pb = new ProcessBuilder("/usr/bin/bash", "-c", cmd);
            Process p = pb.start();
            p.waitFor();
            int exitValue = p.exitValue();
            if (exitValue != 0) {
                System.out.println("Runner exited with value " + exitValue + ". Please check output above for indications of the problem.");
            } else {
                System.out.println("run finished");
            }
            // LOAD result
            ImagePlus plus = IJ.openImage(dirCellposeTmp + File.separator + "image_cp_masks.tif");
            // delete data
            Files.delete(Paths.get(dirCellposeTmp, "image.tif"));
            Path pathMask = Paths.get(dirCellposeTmp, "image_cp_masks.tif");
            if (Files.exists(pathMask))
                Files.delete(pathMask);
            dirCellpose.delete();
            ImageHandler res = plus == null ? null : ImageHandler.wrap(plus);
            if (res != null) {
                res.setOffset(img);
                res.setVoxelSize(img);
            }
            return res;
        } catch (IOException | InterruptedException e) {
            System.out.println("Error cellpose " + e);
            return null;
        }
    }

    public static void main(String[] args) {
        ImageHandler img1 = ImageHandler.wrap(IJ.openImage("/home/thomas/tmpcellpose/image.tif"));
        Cellpose cellpose = new Cellpose();
        cellpose.setPython("/home/thomas/App/miniconda3/envs/cellpose/bin/python"); // HOME
        System.out.println("Python " + cellpose.getPython());

        cellpose.segmentSplitImage(img1, 2, 32).show();
    }
}
