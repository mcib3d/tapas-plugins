package mcib3d.tapas.IJ.plugins.utils;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class SummaryResultsProcess implements TapasProcessingIJ {
    private static final String PROJECT = "project";
    private static final String DATASET = "dataset";
    private static final String IMAGE = "image";
    private static final String DIR = "dir";
    private static final String FILE = "file";

    HashMap<String, String> parameters;
    ImageInfo info;

    public SummaryResultsProcess() {
        parameters = new HashMap<>();
        setParameter(PROJECT, "?project?");
        setParameter(DATASET, "?dataset?");
        setParameter(DIR, "?ij?");
        setParameter(FILE, "?dataset?-summary.csv");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case PROJECT:
                parameters.put(id, value);
                return true;
            case DATASET:
                parameters.put(id, value);
                return true;
            case "name": // deprecated
            case IMAGE:
                parameters.put(id, value);
                return true;
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus imagePlus) {
        // file
        String name = parameters.get(FILE);
        String dir = parameters.get(DIR);
        String name2 = TapasBatchUtils.analyseFileName(name, info);
        String dir2 = TapasBatchUtils.analyseDirName(dir);
        // image
        String image = getParameter(IMAGE);
        String project = getParameter(PROJECT);
        String dataset = getParameter(DATASET);
        String project2 = TapasBatchUtils.analyseFileName(project, info);
        String dataset2 = TapasBatchUtils.analyseFileName(dataset, info);
        String image2 = TapasBatchUtils.analyseFileName(image, info);

        IJ.log("Opening image " + image2 + " in " + project2 + "/" + dataset2);

        ImagePlus label = TapasBatchProcess.inputImage(info, project2, dataset2, image2, info.getC(), info.getT());
        Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(label));

        int nbObjects = population.getNbObjects();
        double volume = population.getObjects3DInt().stream().map(o -> o.size()).reduce(Double::sum).get();

        IJ.log("Found " + nbObjects + " objects for a total volume of " + volume);

        File summary = new File(dir2 + name2);
        boolean newSummary = false;
        if (!summary.exists()) newSummary = true;
        try {
            BufferedWriter bf;
            if (newSummary) {
                bf = new BufferedWriter(new FileWriter(summary));
                bf.write("imageRef,imageLabel,NbObj,volObj\n");
                bf.flush();
            } else bf = new BufferedWriter(new FileWriter(summary, true));
            bf.write(info.getImage() + "," + image2 + "," + nbObjects + "," + volume + "\n");
            bf.flush();
            bf.close();
        } catch (IOException e) {
            System.out.println("Pb summary " + e);
        }

        return imagePlus;
    }

    @Override
    public String getName() {
        return "Summary of number of objects, with total volume";
    }

    @Override
    public String[] getParameters() {
        return new String[]{PROJECT, DATASET, IMAGE, DIR, FILE};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }
}
