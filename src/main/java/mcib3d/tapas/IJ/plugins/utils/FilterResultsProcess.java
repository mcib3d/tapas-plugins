package mcib3d.tapas.IJ.plugins.utils;

import ij.IJ;
import ij.ImagePlus;
import ij.macro.Variable;
import ij.measure.ResultsTable;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import mcib3d.tapas.IJ.TapasProcessingIJ;
import mcib3d.tapas.core.ImageInfo;
import mcib3d.tapas.core.TapasBatchProcess;
import mcib3d.tapas.core.TapasBatchUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class FilterResultsProcess implements TapasProcessingIJ {
    private static final String DIR = "dir";
    private static final String FILE1 = "file";
    private static final String FILE2 = "filteredFile";
    private static final String MIN = "minValue";
    private static final String MAX = "maxValue";
    private static final String DESCRIPTOR = "descriptor";
    private static final String DISCARD_OBJECTS = "discardObjects";


    HashMap<String, String> parameters;
    ImageInfo info;

    public FilterResultsProcess() {
        parameters = new HashMap<>();
        setParameter(DIR, "?ij?");
        setParameter(DISCARD_OBJECTS, "yes");
    }

    @Override
    public boolean setParameter(String id, String value) {
        switch (id) {
            case DIR:
                parameters.put(id, value);
                return true;
            case FILE1:
                parameters.put(id, value);
                return true;
            case FILE2:
                parameters.put(id, value);
                return true;
            case MIN:
                parameters.put(id, value);
                return true;
            case MAX:
                parameters.put(id, value);
                return true;
            case DESCRIPTOR:
                parameters.put(id, value);
                return true;
            case DISCARD_OBJECTS:
                parameters.put(id, value);
                return true;
        }
        return false;
    }

    @Override
    public ImagePlus execute(ImagePlus input) {
        String dir1 = TapasBatchUtils.analyseDirName(parameters.get(DIR));
        String name1 = TapasBatchUtils.analyseFileName(parameters.get(FILE1), info);
        File file1 = new File(dir1 + name1);
        String dir2 = TapasBatchUtils.analyseDirName(parameters.get(DIR));
        String name2 = TapasBatchUtils.analyseFileName(parameters.get(FILE2), info);
        File file2 = new File(dir2 + name2);
        // filtering
        String descriptor = getParameter(DESCRIPTOR);
        double minValue = Double.parseDouble(getParameter(MIN));
        double maxValue = Double.parseDouble(getParameter(MAX));
        // discard objects
        boolean discard = getParameter(DISCARD_OBJECTS).equalsIgnoreCase("yes");
        // no file1 do nothing
        if (!file1.exists()) {
            IJ.log("File " + file1.getAbsolutePath() + " does not exist, exiting");
            return TapasBatchProcess.duplicatePlus(input);
        }
        // extract image and population in case we need to discard some objects
        ImageHandler handler = new ImageShort("no", 1, 1, 1);
        Objects3DIntPopulation population = new Objects3DIntPopulation();
        if (discard) {
            handler = ImageHandler.wrap(input.duplicate());
            population = new Objects3DIntPopulation(handler);
        }
        // Results table
        ResultsTable resultsTable = ResultsTable.getResultsTable();
        if (resultsTable == null) resultsTable = new ResultsTable();
        else resultsTable.reset();
        // reading file1
        HashMap<String, Variable[]> values;
        int nResults1;
        resultsTable = ResultsTable.open2(dir1 + name1);
        nResults1 = resultsTable.size();
        values = extractValuesFromTable(resultsTable);
        // new results table for results
        resultsTable.reset();
        int nrow = -1;
        for (int r = 0; r < nResults1; r++) {
            double valueDesc = values.get(descriptor)[r].getValue();
            if ((valueDesc >= minValue) && (valueDesc <= maxValue)) {
                resultsTable.incrementCounter();
                nrow++;
                for (String header : values.keySet()) {
                    if (values == null) IJ.log("headers1 null " + header);
                    if (values.get(header) == null) IJ.log("headers1.get null " + header);
                    if (values.get(header)[r].getString() != null)
                        resultsTable.setValue(header, nrow, values.get(header)[r].getString());
                    else resultsTable.setValue(header, nrow, values.get(header)[r].getValue());
                }
            } else { // TODO discard corresponding objects
                if (discard) {
                    float label = (float) values.get("Label")[r].getValue();
                    Object3DInt object3DInt = population.getObjectByLabel(label);
                    object3DInt.drawObject(handler, 0);
                }
            }
        }
        // saving results
        try {
            resultsTable.saveAs(file2.getPath());
        } catch (IOException e) {
            IJ.log("Cannot save results " + file2.getPath());
            throw new RuntimeException(e);
        }

        if (discard) return handler.getImagePlus();
        else return input.duplicate();
    }

    @Override
    public String getName() {
        return "Filter results based on descriptor";
    }

    @Override
    public String[] getParameters() {
        return new String[]{DIR, FILE1, FILE2, MIN, MAX, DESCRIPTOR, DISCARD_OBJECTS};
    }

    @Override
    public String getParameter(String s) {
        return parameters.get(s);
    }

    @Override
    public void setCurrentImage(ImageInfo imageInfo) {
        info = imageInfo;
    }

    private int getIndexDescriptor(String descriptor, ResultsTable resultsTable) {
        String[] headers = resultsTable.getHeadings();
        for (int i = 0; i < headers.length; i++) {
            if (headers[i].equalsIgnoreCase(descriptor)) {
                return i;
            }
        }

        return -1;
    }

    private HashMap<String, Variable[]> extractValuesFromTable(ResultsTable resultsTable) {
        HashMap<String, Variable[]> results = new HashMap<>();
        String[] headers = resultsTable.getHeadings();
        for (String h : headers) {
            int c = resultsTable.getColumnIndex(h);
            if (c != -1)
                results.put(h, resultsTable.getColumnAsVariables(h));
        }
        // check if column "image" exists
        if (!resultsTable.columnExists("Image")) {
            Variable[] images = new Variable[resultsTable.size()];
            for (int r = 0; r < resultsTable.size(); r++) {
                images[r] = new Variable(info.getImage());
            }
            results.put("Image", images);
        }
        // check special column label
        if ((resultsTable.size() > 0) && (resultsTable.getLabel(0) != null)) {
            Variable[] labels = new Variable[resultsTable.size()];
            for (int r = 0; r < resultsTable.size(); r++) {
                labels[r] = new Variable(resultsTable.getLabel(r));
            }
            results.put("Label", labels);
        }

        return results;
    }
}
